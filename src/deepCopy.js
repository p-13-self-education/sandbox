export default function deepCopy(obj) {
  return { ...obj };
}

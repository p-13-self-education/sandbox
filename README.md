# Sandbox

**Deprecated**
Moved to [GitHub](https://github.com/OlehSych/sandbox)

### The main purposes of this project

* practice in writing tests
* improve JS skills
* avoid procrastination

### Setup

```shell script
nvm use 12 && npm ci
```
